// Dependencies
const jwt = require ('jsonwebtoken');
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
    console.log(user);

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }  
    
    console.log(data);
    return jwt.sign(data, secret, {})
}

// notes
// 1. You can only get access token when a user logs in in your app with the correct credentials
// 2 as a user , you can only get your own details from your own token from logging in
// 3 JWT is not meant for SENSITIVE DATA.
// 4 JWT is like a passport, you use around the app to access certain features for your type of user


module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

    if (typeof token === "undefined"){
        return res.send({auth: "Failed. No Token"})
    } else {
        token = token.slice(7, token.length)

        jwt.verify(token, secret, (err, decodedToken) => {
            if(err) {
                return res.send({
                        auth: "Failed",
                        message: err.message
                })
            } else {
                req.user = decodedToken
                next();
            }
        })
    }
};


// verifying an admin
module.exports.verifyAdmin = (req,res,next) => {
    if(req.user.isAdmin){
        next();
    } else {
        return res.send({
            auth: "Failed",
            message : "Action Forbidden"
        })
    }
};