const Course = require('../models/Course');

module.exports.addCourse = (req,res) => {
    console.log(req.body);
    let newCourse = new Course ({
        name : req.body.name,
        description : req.body.description,
        price : req.body.price

    }) 
    newCourse.save()
    .then(course => res.send(course))
    .catch(err => res.send (err));
};

module.exports.getAllCourse = (req,res) => {
    Course.find({})
    .then(result => res.send(result))
    .catch(err => res.send(err));
};

// Get single course details

module.exports.getCourseDetails = (req, res) => {
    console.log(req.params);
    /*
        expected id from decoded token
            id: 
            email :
            isAdmin :
    */
    // find the logged in user's documents from our database and send it to the client by its ID

    Course.findById(req.params.id)
    .then(result => res.send(result))
    .catch(err => res.send(err));
};

// UPDATING A COURSE
module.exports.updateCourse = (req,res) => {
    let updates = {
        name : req.body.name,
        description : req.body.description,
        price : req.body.price
    }
    Course.findByIdAndUpdate(req.params.id,updates, {new: true})
    .then(updatedCourse => res.send(updatedCourse))
    .catch(err => res.send(err));
}

// updating course to isActive = false

module.exports.deactivateCourse = (req,res) => {
    let updates = {
        isActive : false
    }
    Course.findByIdAndUpdate(req.params.id, updates, {new: true}).then(deactivatedCourse => res.send(deactivatedCourse)).catch(err => res.send(err));
};
// Activate
module.exports.activateCourse = (req,res) => {
    let updates = {
        isActive : true
    }
    Course.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(activatedCourse => res.send(activatedCourse))
    .catch(err => res.send(err));
};

// Get active courses

module.exports.getActiveCourse = (req, res) => {
    
    Course.find({isActive : true})
    .then(result => {
        if(result !== null  ){
            return res.send(result)
        } else 
            return res.send(err)
    })
    .catch(err => res.send(err));
};

// ! [SECTION] Find courses via its name

module.exports.findCoursesByName = (req, res) => {
    console.log(req.body)

    Course.find({name : {$regex : req.body.name, $options: "$i"}})
    .then(result => {
        if(result.length === 0){
            return res.send('No courses found')
        } else {
            return res.send(result)
        }
    })
}