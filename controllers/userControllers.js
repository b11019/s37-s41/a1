const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../auth')
const Course = require('../models/Course')
const { getMaxListeners } = require('../models/User');
// const secret = secret('')


// REGISTER USER
module.exports.registerUser = (req,res) => {
    console.log(req.body);

    // bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)  -- Salt rounds number of times the characters in the hash randomized
    const hashedPW = bcrypt.hashSync(req.body.password, 10);
    let newUser = new User ({
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email: req.body.email,
        mobileNo : req.body.mobileNo,
        password : hashedPW
    });
    newUser.save()
    .then(user => res.send(user))
    .catch(err => res.send(err));
};

// RETRIEVAL ALL USERS

module.exports.getAllUsers = (req,res) => {
    User.find({})
    .then(result => res.send(result))
    .catch(err => res.send(err));
};

// log in user

module.exports.loginUser = (req, res) => {
    console.log(req.body);
    /* 1. find the user by email
        2. if we found the user, we will check the password
        3. if we dont find the user, then we wil send a message to the client
        4.if upon checking the found user's password is the same as our input password, we will generate the token / key to access our app. if not , we will turn them away by sending a message to the client */

        User.findOne({email: req.body.email})
        .then(foundUser => {
            if(foundUser === null){
                return res.send('No user found in the database')
            }else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
                
                console.log(isPasswordCorrect);
                if(isPasswordCorrect){
                    return res.send({accessToken: auth.createAccessToken(foundUser)})
                } else {
                    return res.send("Incorrect password, please try again")
                }
            }
        })
        .catch(err => res.send(err));
};

// GET DETAILS [SECTION] OF SINGLE USER DETAILS


module.exports.getUserDetails = (req, res) => {
    console.log(req.user);
    /*
        expected id from decoded token
            id: 
            email :
            isAdmin :
    */
    // find the logged in user's documents from our database and send it to the client by its ID

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err));
};

module.exports.checkEmailExist = (req,res) => {
    console.log(req.body);
    User.findOne({email: req.body.email}).then(result => {

        if(result !== null && result.email === req.body.email){
            return res.send('Email already exists')
        } else if (result === null) {

            return res.send ('Email is available')
            // let newTask = new Task({
            //     name : req.body.name,
            //     status : req.body.status
            // })
            // newTask.save().then(result => 
            //     res.send(result))
            //     .catch(error => res.send(error));
        }
    })
    .catch(error => res.send(error))
}

// [SECTION] UPDATING USER DETAILS

module.exports.updateUserDetails = (req,res)=> {
    console.log(req.body);
    console.log(req.user.id); // will check the logged in user if its admin or regular user

    let updates = {
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        mobileNo  : req.body.mobileNo
    }
    User.findByIdAndUpdate(req.user.id, updates, {new: true}) 
    .then(updatedUser => res.send(updatedUser)).catch(err => res.send(err));
};

// Update an Admin

module.exports.updateAdmin = (req,res) => {
    console.log(req.user.id);
    console.log(req.params.id);

    let updates = {
        isAdmin : true
    }
    User.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedUser => res.send(updatedUser)).catch(err => res.send(err));
};


// ! [SECTION] ENROLLMENT

module.exports.enroll = async (req, res) => {
    console.log(req.user.id)
    console.log(req.body.courseId)

    if(req.user.isAdmin){
        return res.send("Action Forbidden")
    } 

    let isUserUpdate = await User.findById (req.user.id).then(user => {
        console.log(user);

        /* id : fsdfa123,
            fN : <uservalue>
            lN : <uservalue>
            email : <uservalue>
            password : <uservalue>
            mobileNo : <uservalue>
            isAdmin : <uservalue>
            enrollments : [{<uservalue>}]
        */
    let newEnrollment = {
        courseId : req.body.courseId
    }
    user.enrollment.push(newEnrollment)

    return user.save()
    .then(user => true)
    .catch(err => err.message)
    })

        if(isUserUpdate !== true){
            return res.send({message : isUserUpdate})
        }

        let isCourseUpdate = await Course.findById(req.body.courseId)
        .then(Course => {
            console.log(Course)
            let enrollee = {
                userId : req.user.id
            }
            Course.enrollees.push(enrollee)
            return Course.save()
            .then (Course => true)
            .catch (err => err.message)
        })
        if (isCourseUpdate !== true){
            return res.send({message : isCourseUpdate})
        }

        if(isUserUpdate && isCourseUpdate){
            return res.send({message : "User enrolled successfully"})
        }
};

/*      
    STEPS: 
        1. Look for the user by its id
            -- push the details of the course we're trying to enroll in
            We'll push to a new subdocument in our user.
        2. Look for the course by its id
            -- push the details of the user who's trying to enroll. We'll push to a new enrollees subdocument in our course
        3. When both saving of docs are successful, we can send a message to the client.

*/

module.exports.getEnrollment = (req, res) => {
    User.findById(req.user.id)
    .then(result => res.send(result.enrollment))
    .catch(err => res.send(err))
};