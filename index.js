// Dependencies Section
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Server Section
const app = express();
const port = 4000;

// DATABASE CONNECTION
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.hpwzu.mongodb.net/course-booking-182?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection error"));
db.once("open", ()=> console.log("Successfully connected to MongoDB"));

// midwares

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Group routing
const userRoutes = require ('./routes/userRoutes');
app.use('/users', userRoutes);

const courseRoutes = require('./routes/courseRoutes');
app.use('/courses', courseRoutes);

// PORT LISTENER

app.listen(port, () => console.log(`Server is running at port ${port}`));

