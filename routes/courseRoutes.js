const express = require ('express');
const router = express.Router();

const courseControllers = require('../controllers/courseControllers');

const auth = require('../auth');
const { route } = require('./userRoutes');
const {verify, verifyAdmin} = auth;

router.post('/',verify, verifyAdmin, courseControllers.addCourse);
router.get('/',courseControllers.getAllCourse);
router.get('/getSingleCourse/:id', courseControllers.getCourseDetails);

// update Course BY ADMIN

router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);

// False IsActive
router.put('/archive/:id', verify,verifyAdmin,courseControllers.deactivateCourse);

// Activate
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);

// get active courses
router.get('/getActiveCourses', courseControllers.getActiveCourse);


// find course by its name
router.post('/search', courseControllers.findCoursesByName);

module.exports = router;