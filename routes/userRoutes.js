// DEPENDENCIES
const express = require ('express');
const router = express.Router();

// IMPORTED MODULES
const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

// object destructuring

const {verify, verifyAdmin} = auth;

//  [SECTION] ROUTES
router.post('/', userControllers.registerUser);

// get all user
router.get('/',userControllers.getAllUsers)

// LOGIN USER

router.post('/login',userControllers.loginUser);

// get user details
router.get ('/getUserDetails', verify ,userControllers.getUserDetails)

// check email
router.get('/checkEmailExists',userControllers.checkEmailExist);


// update users
router.put('/updateUser',verify,userControllers.updateUserDetails);



// MINI ACTIVITY

router.put('/updateUserDetails/:id', verify, auth.verifyAdmin, userControllers.updateAdmin);

router.post('/enroll', verify, userControllers.enroll);

router.get ('/getEnrollment',verify, userControllers.getEnrollment);
module.exports = router;

